var express = require('express');

var app = express();

app.get('/', function (req, res) {
    res.send('hello api');
})

app.listen(3012, function () {
    console.log('API application started successfully');
})